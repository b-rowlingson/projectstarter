Project Sampler
===============

This folder contains a bunch of files for you to get
a quick start writing projects with Markdown and R.

Open the Rproj file in RStudio and look at the `useMarkdown.Rmd`
file for more information.

There is also a `useLaTeX.Rnw` file if you want to write in 
LaTeX instead of Markdown. LaTeX is generally more powerful for
typesetting but can require more time to master.

The documents demonstrate basic text, including figures and tables, 
cross-referencing, bibliography and more.


How to Use
==========

Install a few packages - `knitr`, `rmarkdown`, `bookdown`,
`stargazer`, and `xtable`. You may already have some of them.
They are all on CRAN.

You can then either download this git project via the Download button (next
to "Clone") to get a zip file of the source code, then extract that
zip file on your computer. Open the `starter.Rproj` in RStudio. You
should be able to open `useMarkdown.Rmd` and create the PDF version
for markdown authoring, or open `useLaTeX.Rnw` and knit to PDF if you
want to work with LaTeX.
